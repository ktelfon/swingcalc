import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Calculator calculator = new Calculator();

        JFrame frame = new JFrame("HelloWorldSwing");
//        for (int i = 0; i < 10; i++) {
//            final JLabel label = new JLabel("Hello World");
//            frame.getContentPane().add(label);
//        }

        final JLabel label = new JLabel("0");
        label.setBounds(0, 0, 90, 10);
        frame.add(label);

        int x = 30;
        int y = 30;

        for (int i = 1; i < 11; i++) {
            JButton b = new JButton(String.valueOf(i - 1));
            b.setBounds(x, y, 90, 90);
            x += 125;
            if (i % 3 == 0) {
                y += 125;
                x = 30;
            }
            b.addActionListener((e) ->
            {
                if(label.getText().equals("0")){
                    label.setText(b.getText());
                }else {
                    label.setText(label.getText() + b.getText());
                }
            });
            frame.add(b);
        }

        JButton b = new JButton("+");
        b.setBounds(x, y, 90, 90);
        b.addActionListener((e) ->
        {
            calculator.setFirstNumber(label.getText());
            calculator.setOperator(b.getText());
            label.setText(label.getText() + b.getText());
        });
        frame.add(b);

        JButton equalsButton = new JButton("=");
        equalsButton.setBounds(x + 125, y, 90, 90);
        equalsButton.addActionListener((e) ->
        {
            calculator.setSecondNumber(
                    label.getText()
                            .replace(calculator.getFirstNumber(),"")
                            .replace(calculator.getOperator(),""));

            Integer result =
                    Integer.parseInt(calculator.getFirstNumber()) +
                            Integer.parseInt(calculator.getSecondNumber());
            label.setText(result.toString());
        });

        frame.add(equalsButton);

        frame.setSize(400, 500);

        frame.setLayout(null);

        frame.setVisible(true);
    }
}
